use gdk;
use gtk;
use gtk::prelude::*;
use libhandy;
use lightdm;
use lightdm::{GreeterExt, SessionExt, UserExt, UserListExt};
use std::cell::RefCell;
use std::io::prelude::*;
use std::path::*;
use std::rc::Rc;
use std::{fs, io};

fn main() {
    if gtk::init().is_err() {
        println!("failed to init GTK");
        return;
    }
    libhandy::Keypad::new(true, true);
    let builder = gtk::Builder::new_from_string(include_str!("../interface.ui"));
    let handler = Rc::new(RefCell::new(Handler::new()));
    {
        let mut handler_borrowed = handler.borrow_mut();
        handler_borrowed.get_objects(&builder);
        handler_borrowed.set_username();
        handler_borrowed.setup_greeter(handler.clone());
        handler_borrowed.setup_de_select(handler.clone());
    }
    {
        let handler_borrowed = handler.borrow();
        if let Ok(os_str) = pretty_name() {
            handler_borrowed
                .msg_label
                .as_ref()
                .expect("no msg label")
                .set_text(&os_str);
        }
    }
    {
        let handler_borrowed = handler.borrow();
        let greeter = handler_borrowed.greeter.as_ref().expect("no greeter");
        let user = handler_borrowed.user.as_ref().expect("no user");
        greeter.authenticate(Some(&user)).ok();
    }
    let handler_builder_clone = handler.clone();
    builder.connect_signals(move |_, name| {
        let h_login_cb_clone = handler_builder_clone.clone();
        let h_backspace_clone = handler_builder_clone.clone();
        let h_de_popover_clone = handler_builder_clone.clone();
        match name {
            "on_destry" => Box::new(|_| {
                gtk::main_quit();
                None
            }),
            "login_cb" => Box::new(move |_| {
                let handler = h_login_cb_clone.borrow();
                let pin_entry = handler.pin_entry.as_ref().expect("no pin_entry");
                handler
                    .greeter
                    .as_ref()
                    .expect("no greeter")
                    .respond(&pin_entry.get_text())
                    .ok();
                pin_entry.set_text("");
                None
            }),
            "backspace" => Box::new(move |_| {
                let handler = h_backspace_clone.borrow();
                let pin_entry = handler.pin_entry.as_ref().expect("no pin_entry");
                let length = pin_entry.get_length();
                if length <= 0 {
                    return None;
                }
                pin_entry.delete_text(length - 1, Some(1));
                None
            }),
            "de_popover" => Box::new(move |_| {
                h_de_popover_clone
                    .borrow()
                    .de_select
                    .as_ref()
                    .expect("popover not found")
                    .popup();
                None
            }),
            _ => panic!("unknown signal"),
        }
    });
    let window: gtk::Window = builder
        .get_object("greeter_window")
        .expect("window not found");
    {
        let display = gdk::Display::get_default().expect("cant get display");
        let monitor = display.get_monitor(0).expect("cant get monitor");
        let screen_size = monitor.get_geometry();
        window.set_size_request(screen_size.width, screen_size.height);
        let container: gtk::Box = builder.get_object("container").expect("cant get container");
        container.set_size_request(
            (screen_size.width as f32 * 0.8) as i32,
            (screen_size.height as f32 * 0.8) as i32,
        );
        let style = gtk::CssProvider::new();
        let screen = gdk::Screen::get_default().expect("cant get screen");
        style
            .load_from_data(include_bytes!("../style.css"))
            .expect("failed to load style");
        gtk::StyleContext::add_provider_for_screen(
            &screen,
            &style,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );
        let custom_conf = Path::new("/etc/lightdm/lightdm_mobile_greeter.css");
        if custom_conf.exists() {
            let style2 = gtk::CssProvider::new();
            if let Some(path) = custom_conf.to_str() {
                style2.load_from_path(path).expect("failed to load style2");
            }
            gtk::StyleContext::add_provider_for_screen(
                &screen,
                &style2,
                gtk::STYLE_PROVIDER_PRIORITY_USER,
            )
        }
    }
    window.show();
    gtk::main();
}

#[derive(Default)]
struct Handler {
    user: Option<String>,
    session: Option<String>,
    greeter: Option<lightdm::Greeter>,
    pin_entry: Option<gtk::EntryBuffer>,
    pmpt_label: Option<gtk::Label>,
    msg_label: Option<gtk::Label>,
    de_select: Option<gtk::Popover>,
}

impl Handler {
    fn new() -> Self {
        Default::default()
    }
    fn get_objects(&mut self, builder: &gtk::Builder) {
        self.pin_entry = builder.get_object("entry_buffer");
        self.pmpt_label = builder.get_object("prompt_label");
        self.msg_label = builder.get_object("message_label");
        self.de_select = builder.get_object("de_select");
    }

    fn set_username(&mut self) {
        if let Some(user_list) = lightdm::UserList::get_instance() {
            if let Some(name) = user_list.get_users().first().unwrap().get_name() {
                let name_str = Box::<str>::from(name);
                self.user = Some(String::from(name_str));
            };
        }
    }

    fn setup_greeter(&mut self, handler: Rc<RefCell<Self>>) {
        let greeter = lightdm::Greeter::new();
        let handler_auth_clone = handler.clone();
        greeter.connect_authentication_complete(move |greeter| {
            let handler_borrowed = handler_auth_clone.borrow();
            if greeter.get_is_authenticated() {
                greeter
                    .start_session_sync(Some(&handler_borrowed.session.clone().unwrap()))
                    .ok();
            } else {
                greeter
                    .authenticate(Some(&handler_borrowed.user.clone().unwrap()))
                    .ok();
            }
        });
        let handler_msg_clone = handler.clone();
        greeter.connect_show_message(move |_, msg, _| {
            handler_msg_clone
                .borrow()
                .msg_label
                .as_ref()
                .expect("no msg label")
                .set_text(msg);
        });
        let handler_prmpt_clone = handler.clone();
        greeter.connect_show_prompt(move |_, msg, _| {
            handler_prmpt_clone
                .borrow()
                .pmpt_label
                .as_ref()
                .expect("no msg label")
                .set_text(msg);
        });
        greeter.connect_to_daemon_sync().ok();
        self.greeter = Some(greeter);
    }

    fn setup_de_select(&mut self, handler: Rc<RefCell<Self>>) {
        {
            let session = self
                .greeter
                .as_ref()
                .expect("wheres the greeter at")
                .get_default_session_hint()
                .expect("no default session");
            self.session = Some(String::from(session));
        }
        let vbox = gtk::BoxBuilder::new()
            .orientation(gtk::Orientation::Vertical)
            .visible(true)
            .build();
        let sessions_raw = lightdm::functions::get_sessions();
        let mut sessions = sessions_raw.iter().map(|s| {
            (
                String::from(s.get_name().expect("no name")),
                String::from(s.get_key().expect("no key")),
            )
        });
        let (name, key) = sessions.next().expect("no session");
        let first = gtk::RadioButtonBuilder::new().label(&name).build();
        let mut keys = Vec::new();
        let mut rbs = Vec::new();
        keys.push(key);
        rbs.push(first.clone());
        vbox.add(&first);
        for (name, key) in sessions {
            let key_clone = key.clone();
            let rb = gtk::RadioButton::new_with_label_from_widget(&first, &name);
            keys.push(key);
            rbs.push(rb.clone());
            vbox.add(&rb);
            if self.session.clone().expect("session not found") == key_clone {
                rb.set_active(true);
            }
        }
        let keys_iter = keys.iter();
        for (rb, key) in rbs.iter().zip(keys_iter) {
            let handler_clone = handler.clone();
            let key_clone = key.clone();
            rb.connect_toggled(move |_| {
                handler_clone.borrow_mut().session = Some(key_clone.clone());
            });
        }
        self.de_select.as_ref().expect("no de select").add(&vbox);
        vbox.show_all()
    }
}

fn pretty_name() -> Result<String, io::Error> {
    let file = fs::File::open("/etc/os-release")?;
    let contents = io::BufReader::new(file);
    for line_opt in contents.lines() {
        let line = line_opt?;
        if line.starts_with("PRETTY_NAME=") {
            if let Some(name) = line.split("=").skip(1).next() {
                return Ok(name.trim_matches('"').to_string());
            };
        }
    }
    Err(io::Error::new(
        io::ErrorKind::NotFound,
        "I tried my best :)",
    ))
}
