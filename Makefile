PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
DATADIR ?= $(PREFIX)/share
CONFDIR = /etc

target/release/lightdm-mobile-greeter: src/main.rs
	cargo build --release
build: target/release/lightdm-mobile-greeter
install: build
	install -Dm755 target/release/lightdm-mobile-greeter -t $(DESTDIR)$(BINDIR)
	install -Dm644 lightdm-mobile-greeter.desktop -t $(DESTDIR)$(DATADIR)/xgreeters
