# Lightdm mobile greeter

A simple log in screen for use on touch screens, designed for use on postmarketOS but should work for others too.

## Installing
```
make install
```
## Configuring
As of version 3 I will now try to guess the user.

lightdm.conf
```
greeter-session=lightdm-mobile-greeter
user-session=<THE CHOSEN DE TO LAUNCH AFTER SUCCESSFUL LOGIN>
```

## Theming
If you have a file at `/etc/lightdm/lightdm_mobile_greeter.css` styling will be read from it falling back to the backed in styling, checkout style.css to get ideas on what to change